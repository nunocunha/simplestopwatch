﻿namespace SimpleStopwatch
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.DrawTimer = new System.Windows.Forms.Timer(this.components);
            this.StopwatchLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DrawTimer
            // 
            this.DrawTimer.Interval = 1;
            this.DrawTimer.Tick += new System.EventHandler(this.DrawTimer_Tick);
            // 
            // StopwatchLabel
            // 
            this.StopwatchLabel.BackColor = System.Drawing.Color.Black;
            this.StopwatchLabel.Font = new System.Drawing.Font("Segoe UI", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopwatchLabel.ForeColor = System.Drawing.Color.White;
            this.StopwatchLabel.Location = new System.Drawing.Point(12, 9);
            this.StopwatchLabel.Name = "StopwatchLabel";
            this.StopwatchLabel.Size = new System.Drawing.Size(656, 139);
            this.StopwatchLabel.TabIndex = 0;
            this.StopwatchLabel.Text = "0:00:00.00";
            this.StopwatchLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.StopwatchLabel.Click += new System.EventHandler(this.StopwatchLabel_Click);
            this.StopwatchLabel.DoubleClick += new System.EventHandler(this.StopwatchLabel_DoubleClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 157);
            this.Controls.Add(this.StopwatchLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Simple Stopwatch";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer DrawTimer;
        private System.Windows.Forms.Label StopwatchLabel;
    }
}

