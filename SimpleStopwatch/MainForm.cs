﻿using MovablePython;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleStopwatch
{
    public partial class MainForm : Form
    {
        private Stopwatch Stopwatch = new Stopwatch();

        private Hotkey StartStopHotkey = new Hotkey(Keys.PageUp, false, false, false, true);
        private Hotkey RestartHotkey = new Hotkey(Keys.PageDown, false, false, false, true);

        public MainForm()
        {
            InitializeComponent();

            StartStopHotkey.Pressed += delegate { StartStopWatch(); };

            if (StartStopHotkey.GetCanRegister(this))
            {
                StartStopHotkey.Register(this);
            }

            RestartHotkey.Pressed += delegate { RestartWatch(); };

            if (RestartHotkey.GetCanRegister(this))
            {
                RestartHotkey.Register(this);
            }
        }

        private void StopwatchLabel_DoubleClick(object sender, EventArgs e)
        {
            StopwatchLabel_Click(sender, e);
        }

        private void StopwatchLabel_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;

            if (me.Button == MouseButtons.Left)
            {
                StartStopWatch();
            }
            else if (me.Button == MouseButtons.Right)
            {
                RestartWatch();
            }
        }

        private void DrawTimer_Tick(object sender, EventArgs e)
        {
            UpdateLabelTime();
        }

        private void StartStopWatch()
        {
            if (Stopwatch.IsRunning)
            {
                Stopwatch.Stop();
                DrawTimer.Stop();
            }
            else
            {
                Stopwatch.Start();
                DrawTimer.Start();
            }

            UpdateLabelTime();
        }

        private void RestartWatch()
        {
            if (Stopwatch.IsRunning)
            {
                Stopwatch.Restart();
            }
            else
            {
                Stopwatch.Reset();
            }

            UpdateLabelTime();
        }

        private void UpdateLabelTime()
        {
            TimeSpan Time = Stopwatch.Elapsed;

            int Hours = Time.Days * 24 + Time.Hours;
            int Minutes = Time.Minutes;
            int Seconds = Time.Seconds;
            int Centiseconds = Time.Milliseconds / 10;

            string LabelText = string.Format("{0}:{1:D2}:{2:D2}.{3:D2}", Hours, Minutes, Seconds, Centiseconds);

            StopwatchLabel.Text = LabelText;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (StartStopHotkey.Registered)
            {
                StartStopHotkey.Unregister();
            }

            if (RestartHotkey.Registered)
            {
                RestartHotkey.Unregister();
            }
        }
    }
}
